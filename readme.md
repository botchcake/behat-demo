# Short demo of what behat is all about.

Clone this repo.
Run ``composer install``, to download behat, the mink extension and the goutte driver.
Change the base_url in ``behat.yml`` to a vhost installed on your local machine,
this defaults to test.dev.

Execute the tests by running ``vendor/bin/behat`` from the root of the project.
For running tests with smaller output, running ``vendor/bin/behat -f progress`` prints 
only 1 character per test.
