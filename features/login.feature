Feature: Login
    In order to check that logging in works, I should be able to submit the
    login form on the homepage.

    Scenario:
        Given I am on the homepage
        When I fill in "user" with my username
        And I fill in "pass" with my password
        And I press "submit"
        Then I should see "Logged in as user"

    Scenario:
        Given I am on the homepage
        When I fill in "user" with my username
        And I fill in "pass" with "aap"
        And I press "submit"
        Then I should see "FOUT! MAG NIET"
