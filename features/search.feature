Feature: Search
    In order to check that the search works, I should be able to fill in the
    search field and see the results narrow.

    Scenario:
        Given I am on the homepage
        Then I should see "Article one"
        Then I should see "Article two"
        Then I should see "Article three"

    Scenario:
        Given I am on the homepage
        And I fill in "search" with "one"
        And I press "Search stuff"
        Then I should see "Article one"
        And I should not see "Article two"
        And I should not see "Article three"

    Scenario:
        Given I am on the homepage
        And I fill in "search" with "tattoo"
        And I press "Search stuff"
        Then I should not see "Article one"
        And I should see "Article two"
        And I should see "Article three"
