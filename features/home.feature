Feature: Home
    In order to check that the website still works
    As a website user
    I need to be able to see that the homepage is correct

    Scenario:
        Given I am on the homepage
        Then the response status code should be 200
        And I should see "very test"
        And the page title should be "many test"
        And the page title should not be "AAP"
